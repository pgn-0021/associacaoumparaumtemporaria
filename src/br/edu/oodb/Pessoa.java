package br.edu.oodb;

public class Pessoa {

	private String nome;
	
	private Documento identidade;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Documento getIdentidade() {
		return identidade;
	}

	public void setIdentidade(Documento identidade) {
		this.identidade = identidade;
	}
	
}
